#!/bin/bash -x
DEBUG=4
PUNTODEMONTAJE=/vos/sabras
rdiff-backup -v$DEBUG --print-statistics --exclude ~/.cargo --exclude ~/.local/share/Steam/ --exclude ~/.cache ~ /$PUNTODEMONTAJE/backup-$USER/

rdiff-backup -v$DEBUG --print-statistics --remove-older-than 4W /$PUNTODEMONTAJE/backup-$USER/
rdiff-backup -l /$PUNTODEMONTAJE/backup-$USER/