#!/bin/bash -x
DEBUG=4
PUNTODEMONTAJE=/vos/sabras
rdiff-backup -v$DEBUG --print-statistics \
    --exclude ~/.local/share/Steam/ \
    --exclude ~/.cache \
    --exclude ~/netkit \
    --exclude ~/.steam \
    --exclude ~/snap \
    --exclude ~/.config/google-chrome \
    --exclude ~/Aplicaciones \
    --exclude ~/.codeintel \
    --exclude ~/.dbus \
    --exclude ~/.gvfs \
    ~ "$PUNTODEMONTAJE"/backup-$USER/

echo rdiff-backup -v$DEBUG --print-statistics \
    --remove-older-than 4W \
    "$PUNTODEMONTAJE/backup-$USER/"

rdiff-backup -l "$PUNTODEMONTAJE/backup-$USER/"
